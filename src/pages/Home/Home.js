import classes from './Home.module.css';

import Hero from '../../components/Hero/Hero';
import SecondaryHeader from '../../components/Typography/SecondaryHeader/SecondaryHeader';
import Carousel from '../../components/Carousel/Carousel';

const Home = () => {
  return (
    <main className={classes.home}>
      <Hero />
      <SecondaryHeader>Featured Projects:</SecondaryHeader>
      <Carousel />
    </main>
  );
};

export default Home;
