import Collection from '../../components/Collection/Collection';
import Overlay from '../../components/Overlay/Overlay';
import SecondaryHeader from '../../components/Typography/SecondaryHeader/SecondaryHeader';

import classes from './Portfolio.module.css';

const Portfolio = () => {
  return (
    <main>
      <section className={classes['portfolio__hero']}>
        <Overlay className={classes['portfolio__overlay']}>
          <h1>Here is a collection of all of my best projects.</h1>
          <SecondaryHeader>Hover over each one to learn more!</SecondaryHeader>
        </Overlay>
      </section>
      <Collection />
    </main>
  );
};

export default Portfolio;
