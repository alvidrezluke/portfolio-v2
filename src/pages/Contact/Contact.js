import { useRef, useState } from 'react';
import emailjs from 'emailjs-com';

import classes from './Contact.module.css';

import Form from '../../components/Form/Form';
import Spinner from '../../components/Spinner/Spinner';

const Contact = () => {
  const [contactState, setContactState] = useState('FORM');
  const nameRef = useRef();
  const emailRef = useRef();
  const messageRef = useRef();

  const formSubmissionHandler = (event) => {
    event.preventDefault();
    setContactState('SENDING');
    const nameValue = nameRef.current.value;
    const emailValue = emailRef.current.value;
    const messageValue = messageRef.current.value;
    const templateParams = {
      from_name: nameValue,
      email: emailValue,
      message: messageValue,
    };

    emailjs
      .send(
        'service_u3yegpq',
        'template_7d8anpk',
        templateParams,
        'user_MyaO2QsL6Kumq6wp4ThTs'
      )
      .then((res) => {
        setContactState('SENT');
      })
      .catch((err) => {
        alert(err);
      });
  };

  const contactForm = (
    <Form
      nameRef={nameRef}
      emailRef={emailRef}
      messageRef={messageRef}
      onSubmit={formSubmissionHandler}
    />
  );

  const loadingSpinner = (
    <div className={classes.spinner}>
      <Spinner />
    </div>
  );

  return (
    <main className={classes.contact}>
      {contactState === 'FORM' ? contactForm : null}
      {contactState === 'SENDING' ? loadingSpinner : null}
      {contactState === 'SENT' ? <p>Sent</p> : null}
    </main>
  );
};

export default Contact;
