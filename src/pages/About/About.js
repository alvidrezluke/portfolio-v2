import SecondaryHeader from '../../components/Typography/SecondaryHeader/SecondaryHeader';
import classes from './About.module.css';
import image from './me.png';

const About = () => {
  return (
    <main className={classes.about}>
      <img className={classes['about__image']} src={image} alt='' />
      <div className={classes['about__text']}>
        <SecondaryHeader>I am Luke Alvidrez.</SecondaryHeader>
        <p>
          I design and develop websites using React. I have experience writing
          in the latest versions of HTML, CSS, and JavaScript. I adapt easily
          and understand documentation. I also have experience in Next.js. I am
          a self taught developer from
        </p>
        <p>
          In my free time I enjoy running, reading books, and designing and
          creating 3D printed items. I am based out of Alabama.
        </p>
      </div>
    </main>
  );
};

export default About;
