import { useEffect, useState } from 'react';

import CarouselItem from './CarouselItem/CarouselItem';

import image from '../../assets/images/SchoolCounselingCorner.png';
import classes from './Carousel.module.css';

const Carousel = () => {
  const [carouselMovement, setCarouselMovement] = useState(0);
  const [width, setWidth] = useState(window.innerWidth);

  const handleResize = (event) => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);
  }, []);

  const numberOfSlides = 10;

  const carouselItems = [];
  for (let i = 0; i < numberOfSlides; i++) {
    carouselItems.push(
      <CarouselItem
        key={i}
        image={image}
        title='The School Counseling Corner'
        description='A website to provide high school students with links to important websites.'
        to='theschoolcounselingcorner.com'
      />
    );
  }

  const handleForward = () => {
    if (width > 600) {
      if (carouselMovement > numberOfSlides * -438 + width) {
        setCarouselMovement(carouselMovement - 438);
      } else if (carouselMovement < numberOfSlides * -438 + width) {
        setCarouselMovement(0);
      }
    } else {
      if (carouselMovement > numberOfSlides * width * -1 + width) {
        const newCarouselMovement = carouselMovement - width - 38;
        setCarouselMovement(newCarouselMovement);
      }
    }
  };

  const handleBackward = () => {
    if (width > 600) {
      if (carouselMovement <= -438) {
        const newCarouselMovement = carouselMovement + 438;
        setCarouselMovement(newCarouselMovement);
      }
    } else {
      if (carouselMovement <= -424) {
        const newCarouselMovement = carouselMovement + width + 38;
        setCarouselMovement(newCarouselMovement);
      }
    }
  };

  let active = false;
  let currentX;
  let initialX;
  let xOffset = 0;

  const handleDragStart = (event) => {
    if (event.type === 'touchstart') {
      initialX = event.touches[0].clientX - xOffset;
    } else {
      initialX = event.clientX - xOffset;
    }
    active = true;
  };

  const handleDragEnd = () => {
    initialX = currentX;
    active = false;
  };

  const handleDrag = (event) => {
    if (active) {
      if (event.type === 'touchmove') {
        currentX = event.touches[0].clientX - initialX;
      } else {
        currentX = event.clientX - initialX;
      }
      if (currentX > 10) {
        handleBackward();
      } else if (currentX < -10) {
        handleForward();
      }
      currentX = 0;
    }
  };

  const movementStyles = {
    transform: `translateX(${carouselMovement}px)`,
    transition: `transform 300ms`,
  };

  return (
    <div
      className={classes['carousel__container']}
      onMouseDown={handleDragStart}
      onMouseMove={handleDrag}
      onMouseUp={handleDragEnd}
      onTouchStart={handleDragStart}
      onTouchMove={handleDrag}
      onTouchEnd={handleDragEnd}
    >
      <button
        onClick={handleBackward}
        className={classes['carousel__button--left']}
      >
        &lt;
      </button>
      <section className={classes.carousel} style={movementStyles}>
        {carouselItems}
      </section>
      <button
        onClick={handleForward}
        className={classes['carousel__button--right']}
      >
        &gt;
      </button>
    </div>
  );
};

export default Carousel;
