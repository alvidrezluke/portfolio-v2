import Overlay from '../../Overlay/Overlay';
import SecondaryHeading from '../../Typography/SecondaryHeader/SecondaryHeader';

import classes from './CarouselItem.module.css';

const CarouselItem = (props) => {
  return (
    <div className={classes['carousel-item']}>
      <img
        src={props.image}
        alt=''
        className={classes['carousel-item__image']}
      />
      <Overlay className={classes['carousel-item__overlay']}>
        <SecondaryHeading>{props.title}</SecondaryHeading>
        <p>{props.description}</p>
        <a href={`https://${props.to}`}>Visit!</a>
      </Overlay>
    </div>
  );
};

export default CarouselItem;
