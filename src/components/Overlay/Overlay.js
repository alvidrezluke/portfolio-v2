import classes from './Overlay.module.css';

const Overlay = (props) => {
  const overlayClasses = `${classes.overlay} ${
    props.className ? props.className : ''
  }`;
  return <div className={overlayClasses}>{props.children}</div>;
};

export default Overlay;
