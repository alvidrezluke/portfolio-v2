import classes from './Collection.module.css';
import CollectionItem from './CollectionItem/CollectionItem';
import SecondaryHeader from '../Typography/SecondaryHeader/SecondaryHeader';

const Collection = () => {
  let collectionItems = [];
  for (let i = 0; i < 10; i++) {
    collectionItems.push(
      <CollectionItem key={i} to={'theschoolcounselingcorner.com'}>
        <SecondaryHeader>The School Counseling Corner</SecondaryHeader>
        <p>
          A website to provide high school students with links to important
          websites.
        </p>
        <div className={classes.frameworks}>
          <p>Frameworks used:</p>
          <ul className={classes['framework-list']}>
            <li>React</li>
            <li>Emailjs.com</li>
          </ul>
        </div>
      </CollectionItem>
    );
  }
  return <section className={classes.collection}>{collectionItems}</section>;
};

export default Collection;
