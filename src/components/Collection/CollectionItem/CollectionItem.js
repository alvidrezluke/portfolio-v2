import Overlay from '../../Overlay/Overlay';

import image from '../../../assets/images/SchoolCounselingCorner.png';

import classes from './CollectionItem.module.css';

const CollectionItem = (props) => {
  return (
    <div className={classes.item}>
      <img src={image} alt='' className={classes['item__image']} />
      <Overlay className={classes['item__overlay']}>
        {props.children}
        <a href={`https://${props.to}`}>Visit!</a>
      </Overlay>
    </div>
  );
};

export default CollectionItem;
