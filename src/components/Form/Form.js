import classes from './Form.module.css';

const Form = (props) => {
  return (
    <form onSubmit={props.onSubmit} className={classes.form}>
      <label htmlFor='name'>Name:</label>
      <input id='name' type='text' ref={props.nameRef} />
      <label htmlFor='email'>Email:</label>
      <input id='email' type='email' ref={props.emailRef} />
      <label htmlFor='message'>Message:</label>
      <textarea id='message' type='text' ref={props.messageRef} />
      <button type='submit'>Submit</button>
    </form>
  );
};

export default Form;
