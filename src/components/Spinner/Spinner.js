import classes from './Spinner.module.css';

const Spinner = (props) => {
  const spinnerClasses = `${classes['lds-ring']} ${
    props.className ? props.className : ''
  }`;
  return (
    <div className={spinnerClasses}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Spinner;
