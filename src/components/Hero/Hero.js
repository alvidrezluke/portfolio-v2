import Overlay from '../Overlay/Overlay';

import heroImage from './hero-image-medium.jpg';
import classes from './Hero.module.css';

const Hero = () => {
  return (
    <section className={classes.hero}>
      <img src={heroImage} alt='' className={classes['hero__image']} />
      <Overlay className={classes['hero__overlay']}>
        <h1>
          Hi! I'm Luke Alvidrez,
          <br />a web designer and React developer.
        </h1>
      </Overlay>
    </section>
  );
};
export default Hero;
