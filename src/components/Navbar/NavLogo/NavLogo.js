import logo from './NavLogo.png';
import classes from './NavLogo.module.css';

const Logo = (props) => {
  return (
    <div
      className={classes['nav-logo']}
      onClick={props.onClick ? props.onClick : () => {}}
    >
      <img src={logo} alt='Luke Alvidrez Web Design' />
    </div>
  );
};

export default Logo;
