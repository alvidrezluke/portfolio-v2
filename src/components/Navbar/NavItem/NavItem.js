import { NavLink } from 'react-router-dom';
import classes from './NavItem.module.css';

const NavItem = (props) => {
  return (
    <li>
      <NavLink
        exact={props.exact}
        to={props.to}
        className={classes.navlink}
        activeClassName={classes['navlink--active']}
        onClick={props.onClick}
      >
        {props.name}
      </NavLink>
    </li>
  );
};

export default NavItem;
