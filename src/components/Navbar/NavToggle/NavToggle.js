import classes from './NavToggle.module.css';

const NavToggle = (props) => {
  const navToggleClasses = `${classes['nav-toggle']} ${
    props.className ? props.className : ''
  }`;

  return (
    <div className={navToggleClasses}>
      <input
        type='checkbox'
        className={classes['nav-toggle__checkbox']}
        id='navi-toggle'
        onClick={props.onClick}
        ref={props.checkboxRef}
      />
      <label htmlFor='navi-toggle' className={classes['nav-toggle__button']}>
        <span className={classes['nav-toggle__icon']}>&nbsp;</span>
      </label>
    </div>
  );
};

export default NavToggle;
