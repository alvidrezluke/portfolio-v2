import NavLogo from './NavLogo/NavLogo';
import NavItem from './NavItem/NavItem';
import NavToggle from './NavToggle/NavToggle';

import classes from './Navbar.module.css';
import { useState, useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';

const Navbar = () => {
  const [showMobileNav, setShowMobileNav] = useState(false);
  const checkRef = useRef();
  const handleTogglePressed = () => {
    const currentState = showMobileNav;
    setShowMobileNav(!currentState);
  };
  const navigateToNewPage = () => {
    handleTogglePressed();
    checkRef.current.checked = false;
  };
  let navigationLinksClasses = `${classes['navigation__links']}`;
  if (showMobileNav) {
    navigationLinksClasses = `${classes['navigation__links']} ${classes['navigation__links--shown']}`;
  }
  const history = useHistory();

  const goHome = () => {
    history.push('/');
  };

  return (
    <nav className={classes.navigation}>
      <Link to='/'>
        <NavLogo onClick={goHome} />
      </Link>
      <NavToggle
        className={classes['navigation__toggle']}
        onClick={handleTogglePressed}
        checkboxRef={checkRef}
      />
      <ul className={navigationLinksClasses}>
        <NavItem exact name='Home' to='/' onClick={navigateToNewPage} />
        <NavItem name='Portfolio' to='/portfolio' onClick={navigateToNewPage} />
        <NavItem name='About' to='/about' onClick={navigateToNewPage} />
        <NavItem name='Contact' to='/contact' onClick={navigateToNewPage} />
      </ul>
    </nav>
  );
};

export default Navbar;
