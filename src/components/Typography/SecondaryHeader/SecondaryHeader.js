import classes from './SecondaryHeader.module.css';

const SecondaryHeader = (props) => {
  return <h2 className={classes['secondary-header']}>{props.children}</h2>;
};

export default SecondaryHeader;
